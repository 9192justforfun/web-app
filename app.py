#c_env\scripts\activate.bat -->
from flask import Flask, render_template, request
import pickle
import numpy as np

#print("Succesfully installed")


app = Flask(__name__)

@app.route("/", methods=["GET", "POST"])
def home():
    if request.method == "POST":
        with open("customer_seg_model.pkl", "rb") as f:
            model = pickle.load(f)
        
        annual_income = float(request.form["annual_income"])
        spending_score = float(request.form["spending_score"])
        
        data = np.array([[annual_income, spending_score]])
        res = model.predict(data)
        
        return render_template("home.html", msg=f"Your Given Customer Lies In Cluster : {res[0] + 1}")

    # Render the initial form
    return render_template("home.html")


# if __name__ == '__main__':
#     app.run(debug=True)

if __name__ == "__main__":
    app.run(host="0.0.0.0", port=8080)  