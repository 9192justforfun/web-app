FROM python
# Define a base image for your application. Choose the appropriate image based on your application's requirements.
#RUN apt  install python3-pip -y
WORKDIR /app
RUN apt update

RUN pip install flask
RUN pip install pandas
RUN pip install scikit-learn
# Set a working directory for your application code
 
# Copy your application code from the local directory into the container
COPY . /app
 
# Expose the port your application listens on
EXPOSE 8080
# Specify the command to run when the container starts (replace with your actual application entrypoint)
# CMD [ "python3", "-m" , "flask" , "run", "--host=0.0.0.0"]
CMD [ "python3", "app.py"]
